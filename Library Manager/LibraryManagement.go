// todo display book
// todo lend book 
// todo add book
// todo return book

package main

import ("fmt")

type libManageSys struct {
	libraryName string
	bookList []string
	lenders map[string]string
	donors map[string]string
}

func indexOf(element string, data []string) (int) {
	for k, v := range data {
		if element == v {
			return k
		}
	}
	return -1    //not found.
}

func (l *libManageSys) displayBooks() {
	fmt.Println("The books available are:")
	for _, book := range l.bookList {
		fmt.Println(book)
	}
}

func (l *libManageSys) addBook(book, donor string) {
	l.donors[donor] = book
	l.bookList = append(l.bookList, book)
	fmt.Printf("%v, your book '%v' has been donated successfully into our system! Thanks for your contribution!\n", donor, book)
	// fmt.Println(l.bookList)
}

func (l *libManageSys) lendBook(book, lender string) {
	for _, avBook := range l.bookList {
		if avBook == book {
			// fmt.Print("Enter your name: ")
			// fmt.Scanln(&lender)
			index := indexOf(book, l.bookList)
			if index == -1 {
				fmt.Println("No such book in the booklist!")
				continue
			} else {
				newBooklist := l.bookList[:index]
				newBooklist = append(newBooklist, l.bookList[index+1:]...)
				l.bookList = newBooklist
				l.lenders[lender] = book
				fmt.Printf("%v, the book '%v' has been lended to you successfully! Return it within 15 days!\n", lender, book)
			}
		}
	}
}

func (l *libManageSys) returnBook(lender string) {
	_, ok := l.lenders[lender]
	if ok {
		l.bookList = append(l.bookList, l.lenders[lender])
		delete(l.lenders, lender)
		fmt.Println("Book has been returned successfully!")
		
	} else {
		fmt.Println("No such lender in the system! Try putting proper name.")
	}
}

func main() {
	fmt.Println("!!WELCOME TO THE GO LIBRARY MANAGEMENT SYSTEM!!")

	lms := libManageSys{libraryName:"GoTestLibrary", 
						bookList:[]string{"Competing in the Age of AI", "Object Oriented Programming with Python", "The Silva Mind Control Method"}, 
						lenders:map[string]string{}, 
						donors:map[string]string{}}

	for {
		fmt.Println("What do you want to do:\n 1. See Available Books \n 2. Donate a Book \n 3. Lend a Book \n 4. Return a Book")

		var task int
		fmt.Scanln(&task)
		if task == 1 {
			lms.displayBooks()

		} else if task == 2 {
			var book, donor string
			fmt.Print("Enter the book name you want to donate: ")
			fmt.Scanln(&book)
			fmt.Print("Enter your name: ")
			fmt.Scanln(&donor)
			lms.addBook(book, donor)

		} else if task == 3 {
			var book, lender string
			fmt.Print("Enter the book name you want to lend: ")
			fmt.Scanln(&book)
			fmt.Print("Enter your name: ")
			fmt.Scanln(&lender)
			lms.lendBook(book, lender)

		} else if task == 4 {
			var returner string
			fmt.Print("Enter your name: ")
			fmt.Scanln(&returner)
			lms.returnBook(returner)

		} else {
			fmt.Println("Invalid input!")
		}

		fmt.Println("\n````````````````````````````````````````\n")
	}
}