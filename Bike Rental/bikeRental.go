/*
Customers can:
todo See available bikes on the shop. done
todo Rent bikes on hourly basis $5 per hour. done 
todo Rent bikes on daily basis $20 per day.  done
todo Rent bikes on weekly basis $60 per week. done
todo Family Rental, a promotion that can include from 3 to 5 Rentals (of any type) with a discount of 30% of the total price. done

The bike rental shop can:
*Issue a bill when customer decides to return the bike. 
*Display available inventory. 
*Take requests on hourly, daily and weekly basis by cross verifying stock. 


For simplicity we assume that:
* Any customer requests rentals of only one type i.e hourly, monthly or weekly. done
* Is free to choose the number of bikes he/she wants. done
* Requested bikes should be less than available stock. done
*/

package main

import (
	"fmt"
	"strings")

type bikeRental struct {
	noOfBikes int
	users []triDict
}

type triDict struct {
	name string
	mode string
	bikes int
}

func (br bikeRental) displayBikes() {
	fmt.Println(br.noOfBikes)
}

func (br *bikeRental) rentBike(mode string, name string, bikes int) {

	data := triDict{name:name, mode:mode, bikes:bikes}
	//// fmt.Println("Minusing bikes...")
	br.noOfBikes -= bikes
	//// fmt.Println("Updating data...")
	newusers := append(br.users, data)
	//// fmt.Println("Bike rented successfully!")
	br.users = newusers
	fmt.Println(br.users)
	fmt.Println(br.noOfBikes)
}

func (br *bikeRental) issueBill(name string) {
	fmt.Println(br.users)
	for _, bills := range br.users {
		if strings.ToLower(bills.name) == strings.ToLower(name) {
			if bills.mode == "hourly" {
				amount := 5 * bills.bikes
				fmt.Printf("\n-------------------------\n Name: %v\n Bikes Rented: %v\n Rental mode: %v\n Total amount payable: %v$.\n", bills.name, bills.bikes, strings.ToTitle(bills.mode), amount)
				br.noOfBikes += bills.bikes

			} else if bills.mode == "daily" {
				amount := 20 * bills.bikes
				fmt.Printf("\n-------------------------\n Name: %v\n Bikes Rented: %v\n Rental mode: %v\n Total amount payable: %v$.\n", bills.name, bills.bikes, strings.ToTitle(bills.mode), amount)
				br.noOfBikes += bills.bikes

			} else if bills.mode == "weekly" {
				amount := 60 * bills.bikes
				fmt.Printf("\n-------------------------\n Name: %v\n Bikes Rented: %v\n Rental mode: %v\n Total amount payable: %v$.\n", bills.name, bills.bikes, strings.ToTitle(bills.mode), amount)
				br.noOfBikes += bills.bikes

			} else {
				fmt.Println("Error in something")
			}
			
		} else {
			fmt.Println("No match!")
		}
	}
}

func main()  {
	fmt.Println("Welcome to the Go Bike Rental System!")
	goBikeRental := bikeRental{noOfBikes:50, users:[]triDict{}}

	for {
		fmt.Println("What do you want to do:\n 1. See Available Bikes \n 2. Rent a Bike \n 3. Return a Bike")

		var task int
		fmt.Scanln(&task)
	
		if task == 1 {
			goBikeRental.displayBikes()

		} else if task == 2 {
			fmt.Print("Enter your name: ")
			var name string
			fmt.Scanln(&name)

			fmt.Print("Enter number of bikes to rent: ")
			var bikes int
			fmt.Scanln(&bikes)
			if bikes > goBikeRental.noOfBikes {
				fmt.Printf("These much bikes not available! You can now at max rent %v bikes!", goBikeRental.noOfBikes)
				continue
			}

			fmt.Print("Enter rental mode(hourly/daily/weekly): ")
			var mode string
			fmt.Scanln(&mode)
			mode = strings.ToLower(mode)
			fmt.Println(mode)

			if mode == "hourly" || mode == "daily" || mode == "weekly" {
				//// fmt.Println("executing function")
				goBikeRental.rentBike(mode, name, bikes)
				//// fmt.Println("executed")
			} else {
				fmt.Println("Invalid input")
				continue
			}
			

		} else if task == 3 {
			var name string
			fmt.Print("Enter your name: ")
			fmt.Scanln(&name)

			goBikeRental.issueBill(name)
			
		} else {
			fmt.Println("Invalid input!")
		}

	}
}